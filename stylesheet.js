import { StyleSheet } from 'react-native';
const styles = StyleSheet.create({
  app: {
    backgroundColor: '#38232f',
    alignSelf: 'stretch',
  },
  header: {
    backgroundColor: '#e6d5a1',
    height: 110,
    paddingLeft: 40
  },
  logo:{
    height:94,
    width:128,
    position: 'relative',
    bottom: 20,
    marginBottom: -28,
    resizeMode: 'contain',
    width: 92,
    bottom:5
  },
  topadvertisement: {
    position: 'relative',
    left: 300,
    bottom: 50,
    height: 64,
    bottom: 80,
    width: '51%',
  },
  soundon: {
    position: 'relative',
    left: 400,
    zIndex: 3,
  },
  player: {
    width: 900,
    height: 500,
    position:'relative',
    left: 350,
    bottom: 680,
  },
  topics: {
    backgroundColor: '#e6d5a1',
    width: '23%',
  },
  topic: {
    fontSize:22,
    paddingLeft:32,
    paddingTop:5,
    borderColor: '#cbae57',
    borderWidth: 2,
    borderRadius: 4,
  },
  topicTitle: {
    fontSize:24,
    paddingLeft:8,
    fontWeight:'bold',
    paddingTop:5,
    paddingBottom:10,
  },
  thumbnail1: {
    height:170,
    width:280,
    zIndex:3,
    position:'relative',
    left:350,
  },
  thumbnail2: {
    height:170,
    width:280,
    zIndex:3,
    position:'relative',
    left:375,
  },
  thumbnail3: {
    height:170,
    width:280,
    zIndex:3,
    position:'relative',
    left:400,
  }
});

module.exports = styles;