const videos = [
        {src:require('../assets/videos/asthma-1.mp4'),name:'asthma-1'},
        {src:require('../assets/videos/asthma-2.mp4'),name:'asthma-2'},
        {src:require('../assets/videos/asthma-3.mp4'),name:'asthma-3'},
        {src:require('../assets/videos/copd-1.mp4'),name:'copd-1'},
        {src:require('../assets/videos/copd-2.mp4'),name:'copd-2'},
        {src:require('../assets/videos/copd-3.mp4'),name:'copd-3'},
        {src:require('../assets/videos/diabetes-1.mp4'),name:'diabetes-1'},
        {src:require('../assets/videos/diabetes-2.mp4'),name:'diabetes-2'},
        {src:require('../assets/videos/diabetes-3.mp4'),name:'diabetes-3'},
        {src:require('../assets/videos/emphysema-1.mp4'),name:'emphysema-1'},
        {src:require('../assets/videos/emphysema-2.mp4'),name:'emphysema-2'},
        {src:require('../assets/videos/emphysema-3.mp4'),name:'emphysema-3'},
        {src:require('../assets/videos/hyperlipidemia-1.mp4'),name:'hyperlipidemia-1'},
        {src:require('../assets/videos/hyperlipidemia-2.mp4'),name:'hyperlipidemia-2'},
        {src:require('../assets/videos/hyperlipidemia-3.mp4'),name:'hyperlipidemia-3'},
        {src:require('../assets/videos/hypertension-1.mp4'),name:'hypertension-1'},
        {src:require('../assets/videos/hypertension-2.mp4'),name:'hypertension-2'},
        {src:require('../assets/videos/hypertension-3.mp4'),name:'hypertension-3'},
        {src:require('../assets/videos/narcolepsy-1.mp4'),name:'narcolepsy-1'},
        {src:require('../assets/videos/narcolepsy-2.mp4'),name:'narcolepsy-2'},
        {src:require('../assets/videos/narcolepsy-3.mp4'),name:'narcolepsy-3'},
        {src:require('../assets/videos/pneumonia-1.mp4'),name:'pneumonia-1'},
        {src:require('../assets/videos/pneumonia-2.mp4'),name:'pneumonia-2'},
        {src:require('../assets/videos/pneumonia-3.mp4'),name:'pneumonia-3'}
        ]

module.exports = videos;

