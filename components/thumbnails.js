import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, Button, TouchableNativeFeedback  } from 'react-native';
import Player from './player.js';
import styles from '../stylesheet.js';

let baseurl = '../assets/thumbnails/';
let extension = '.jpeg';



export default class Thumbnails extends Component {
  constructor(props){
    super(props);
    this.state = {
      topics: [
        {src:require('../assets/thumbnails/asthma-1.jpeg'),name:'asthma-1'}, 
        {src:require('../assets/thumbnails/asthma-2.jpeg'),name:'asthma-2'},
        {src:require('../assets/thumbnails/asthma-3.jpeg'),name:'asthma-3'},
        {src:require('../assets/thumbnails/copd-1.jpeg'),name:'copd-1'}, 
        {src:require('../assets/thumbnails/copd-2.jpeg'),name:'copd-2'},
        {src:require('../assets/thumbnails/copd-3.jpeg'),name:'copd-3'},
        {src:require('../assets/thumbnails/diabetes-1.jpeg'),name:'diabetes-1'}, 
        {src:require('../assets/thumbnails/diabetes-2.jpeg'),name:'diabetes-2'},
        {src:require('../assets/thumbnails/diabetes-3.jpeg'),name:'diabetes-3'},
        {src:require('../assets/thumbnails/emphysema-1.jpeg'),name:'emphysema-1'}, 
        {src:require('../assets/thumbnails/emphysema-2.jpeg'),name:'emphysema-2'},
        {src:require('../assets/thumbnails/emphysema-3.jpeg'),name:'emphysema-3'},
        {src:require('../assets/thumbnails/hyperlipidemia-1.jpeg'),name:'hyperlipidemia-1'}, 
        {src:require('../assets/thumbnails/hyperlipidemia-2.jpeg'),name:'hyperlipidemia-2'},
        {src:require('../assets/thumbnails/hyperlipidemia-3.jpeg'),name:'hyperlipidemia-3'},
        {src:require('../assets/thumbnails/hypertension-1.jpeg'),name:'hypertension-1'}, 
        {src:require('../assets/thumbnails/hypertension-2.jpeg'),name:'hypertension-2'},
        {src:require('../assets/thumbnails/hypertension-3.jpeg'),name:'hypertension-3'},
        {src:require('../assets/thumbnails/narcolepsy-1.jpeg'),name:'narcolepsy-1'}, 
        {src:require('../assets/thumbnails/narcolepsy-2.jpeg'),name:'narcolepsy-2'},
        {src:require('../assets/thumbnails/narcolepsy-3.jpeg'),name:'narcolepsy-3'},
        {src:require('../assets/thumbnails/pneumonia-1.jpeg'),name:'pneumonia-1'}, 
        {src:require('../assets/thumbnails/pneumonia-2.jpeg'),name:'pneumonia-2'},
        {src:require('../assets/thumbnails/pneumonia-3.jpeg'),name:'pneumonia-3'}
      ],
      items: [{src:require('../assets/thumbnails/asthma-1.jpeg'),name:'asthma-1'}, 
        {src:require('../assets/thumbnails/asthma-2.jpeg'),name:'asthma-2'},
        {src:require('../assets/thumbnails/asthma-3.jpeg'),name:'asthma-3'}
      ],
      video: 'asthma-1'
    }
    this.setThumbnails = this.setThumbnails.bind(this);
  }
  componentWillReceiveProps(nextProps){
    let newtopic = nextProps.current.toLowerCase();
    this.setThumbnails(newtopic);
  }
  chooseVid(video){
    this.setState({video,})
  }

  setThumbnails(item){
     let topics = [];
    this.state.topics.forEach((topic)=>{
        if(topic.name.includes(item)){
          topics.push(topic);
        }
    })
    this.setState({items:topics, video: topics[0].name});
  }

  render() {
    const thumbnails = this.state.items.map((vid,i) => {
      return(<TouchableNativeFeedback  key={"th" + i} onPress={()=>this.chooseVid(vid.name)}>
              <Image
                source={vid.src}
                key={"thumbnail"+i}
                style={styles["thumbnail"+(i+1)]} />
            </TouchableNativeFeedback >)
      })
    return(
      <View>
        <View style={{flexDirection:'row'}}>
          {thumbnails}
        </View>
        <Player video={this.state.video} setThumbnails={this.setThumbnails} />
      </View>
    )
  }
}
