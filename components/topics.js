import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, Button } from 'react-native';
import styles from '../stylesheet.js';
import Thumbnails from './thumbnails.js';


export default class Topics extends Component {
  constructor(props){
    super(props);
    this.state = {
      topics: ['Asthma','COPD','Diabetes','Emphysema','Hyperlipidemia','Hypertension','Narcolepsy','Pneumonia'],
      current: 'Asthma',
      copayneeded: false
    }
  }
  chooseCurrent(topic){
    this.setState({current: topic});
  }
  render(){
    const topics = this.state.topics.map((topic,i) => {
      return(
          <Text 
             style={styles.topic} 
             key={"topic" + i}
             onPress={()=> this.chooseCurrent(topic)}>
             {topic}
         </Text>
         )
    })
    return(
      <View>
        <View style={styles.topics}>
        <Text style={styles.topicTitle}> Choose Your Topic</Text>
        {topics}
        <Text style={styles.topic}>News</Text>
        <Text style={styles.topic}>Sports</Text>
        <Text style={styles.topic}>Entertainment</Text>
        <Text style={styles.topicTitle}>Applications</Text>
        </View>
        <Thumbnails current={this.state.current}/>
      </View>
    )
  }
}
