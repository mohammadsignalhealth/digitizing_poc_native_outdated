import React, { Component } from 'react';
import { View, Image, TouchableNativeFeedback } from 'react-native';
import { Video } from 'expo';
import videos from '../jsonassets/videos.js';
import styles from '../stylesheet.js';

export default class Player extends Component {
  constructor(props) {
    super(props);
    this.state = {
      playlist: videos,
      ms: 0,
      controls: false,
    };
  }
  componentWillReceiveProps(nP) {
    this.adjustPlaylist(nP.video);
  }
  adjustPlaylist(topic) {
    let i = 0,
      playlist = this.state.playlist;
    while (!playlist[0].name.includes(topic)) {
      let temp = playlist.shift();
      playlist.push(temp);
      i++;
    }
    this.setState({ playlist });
  }

  videoUpdated(playbackStatus) {
    if (playbackStatus['didJustFinish']) {
      this.setState({ ms: 500 });
      this.playNext();
    }
  }
  componentWillUpdate(nextProps, nextState) {
    if (nextState['playlist']) {
      let word = nextState['playlist'][0].name;
      let revised = word.substr(0, word.indexOf('-'));
      if (!nextProps['video'].includes(revised)) {
        nextProps.setThumbnails(revised);
      }
    }
  }
  playNext() {
    // playNext() method takes the first item in the this.state.playlist array and moves it to the back
    let playlist = this.state.playlist;
    let temp = playlist[0];
    playlist.shift();
    playlist.push(temp);
    this.setState({ ms: 500, controls: false });
    this.setState({ playlist });
  }
  loadedFine() {
    this.setState({ ms: 0 });
    setTimeout(() => this.setState({ controls: true }), 1000);
  }
  render() {
    return (
      <View>
        <Video
          ref={this._handleVideoRef}
          onPlaybackStatusUpdate={playbackStatus =>
            this.videoUpdated(playbackStatus)}
          shouldPlay={true}
          onLoad={() => this.loadedFine()}
          positionMillis={this.state.ms}
          useNativeControls={this.state.controls}
          source={this.state.playlist[0].src}
          volume={1.0}
          rate={1.0}
          resizeMode="cover"
          style={styles.player}
        />
      </View>
    );
  }
}
