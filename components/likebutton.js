import React, { Component } from 'react';
import { Image, TouchableNativeFeedback, Text, View } from 'react-native';
let styles = require('../stylesheet.js');


export default class Likebutton extends Component {
  constructor(props){
    super(props);
    this.state = {
      liked: false
    }
  }
  heart(){
    this.setState({liked: !this.state.liked})
  }
  render() {
    let heart = () => {
      if(this.state.liked){
        return(<TouchableNativeFeedback onPress={()=> this.heart()}>
                <Image source={require('../assets/images/heart.png')} style={{width:50,height:40,marginLeft:5}}/>
              </TouchableNativeFeedback>)
      }else{
        return(<TouchableNativeFeedback onPress={()=> this.heart()}>
                <View>
                  <Image source={require('../assets/images/whiteheart.png')} style={{width:50,height:40,position:'relative',left:10}} />
                  <Text>Like this video</Text>
                </View>
              </TouchableNativeFeedback>)
      }
    }
    return(
      <View style={{zIndex:2,position:'absolute',left:1030,top:40}}> 
        {heart()}
      </View>
      )
  }
}
