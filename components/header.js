import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, Button } from 'react-native';
import Likebutton from './likebutton.js';

let styles = require('../stylesheet.js');


export default class Header extends Component {
  render() {
    return(
       <View style={styles.header}>
          <Text style={{position:'relative',top:20}}>Western Kentucky</Text> 
          <Text style={{position:'relative',top:20}}>Pulmonary Clinic</Text>
          <Image source={require('../assets/images/Signal_Logo.png')} style={styles.logo} />
          <Image source={require('../assets/images/3.png')} style={styles.topadvertisement}  />
          <Likebutton />
        </View>
      )
  }
}


